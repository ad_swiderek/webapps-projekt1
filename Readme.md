-klonujemy aplikację wklejając do konsoli "git clone https://ad_swiderek@bitbucket.org/ad_swiderek/webapps-projekt1.git"
-pobieramy wymagane paczki: "npm install" -> "npm install --save cors" -> "npm install --save socket.io" 
-uzupełniamy login i hasło
-uruchamiamy aplikację: node dialer.js
-wchodzimy na stronę https://restninja.io/ 
	-w prawym górnym rogu ustawiamy "ajax"
	-ustawiamy metodę na POST
	-request uri: http://localhost:3000/call
	-ustawiamy body na json
	{
	"number1": "<pierwszy nr telefonu>",
	"number2": "<drugi nr telefonu>"
	}
	-wchodzimy w sekcję headers i ustawiamy content-type | application/json
	-klikamy send
	-jeśli operacja zostanie zakończona sukcesem po prawej stronie zobaczymy nasze "callId", które możemy użyc do zweryfikowania statusu połączenia (zad 2) poprzez wpisanie w przeglądarce http://localhost:3000/status/nasze_callId

-wchodzimy na stronę https://amritb.github.io/socketio-client-tool/
	-w polu "Socket.io server URL" wpisujemy: http://localhost:3000
	-w zakładce "Listening" w polu event name wpisujemy "status" i klikamy Listen a następnie Connect w prawym górym rogu
	-teraz w oknie "On "status" Events" będą pojawiać się statusy naszego połączenia (zad 1)
