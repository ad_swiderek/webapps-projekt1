const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ConnectorFactory = require('dialer').ConnectorFactory;

let bridge = null;


const config = {
    url: 'https://uni-call.fcc-online.pl',
    login: '<login>',   
    password: '<haslo>'
};

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());

http.listen(3000, () => {
    console.log('app listening on port 3000');
});

io.on('connection', (socket) => {
    console.log('a user connected')
    socket.on('disconnect', () => {
        console.log('a user disconnected')
    })
    socket.on('message', (message) => {
        console.log('message', message)
    })
    io.emit('message', 'connected!')

})

app.get('/call/:number1/:number2', async (req, res) => {
    const number1 = req.params.number1;
    const number2 = req.params.number2;
    bridge = await Dialer.call(number1, number2);
    res.json({ success: true });
})

app.get('/status', async (req, res) => {
    let status = 'NONE';
    if (bridge !== null) {
        status = await bridge.getStatus();
    }
    res.json({ success: true, status: status });
});

/*app.post('/call', async (req, res) => {
    const body = req.body;
    console.log(body);
    bridge = await Dialer.call(body.number1, body.number2);
    let oldStatus = null
    let interval = setInterval(async () => {
        let currentStatus = await bridge.getStatus();
        if (currentStatus !== oldStatus) {
            oldStatus = currentStatus
            io.emit('message', currentStatus)
        }
    }, 500)

    res.json({ success: true });
})*/

//Zad 1:
app.post('/call', async (req, res) => {
    const body = req.body;
    let connector = ConnectorFactory.create(config);
    bridge = await connector.call(body.number1, body.number2);
    let callId = bridge.id;

    let oldStatus = null
    let interval = setInterval(async () => {
        let currentStatus = await connector.getStatus(callId);
        if (currentStatus.status !== oldStatus) {
            oldStatus = currentStatus.status
            io.emit('status', currentStatus.status)
        }
        if (currentStatus.status == `ANSWERED` || currentStatus.status == `FAILED` || currentStatus.status == `BUSY` || currentStatus.status == `NO ANSWER`) {
            clearInterval(interval)
        }
    }, 1000)


    res.json({ success: true, callId: callId });
})

//Zad 2:
app.get('/status/:callsId', async (req, res) => {
    let status = 'NONE';
    let connector = ConnectorFactory.create(config);
    const callId = req.params.callsId;
    status = await connector.getStatus(callId);
    res.json({ success: true, status: status.status });
});